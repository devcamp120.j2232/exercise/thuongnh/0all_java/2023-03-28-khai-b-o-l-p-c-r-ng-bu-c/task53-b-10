import com.devcamp.task53.Author;
import com.devcamp.task53.Book;
import com.devcamp.task53.b20.Circle;
import com.devcamp.task53.b20.Cylinder;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        /*
        Author  author1 = new Author("Hieu","hieuhn@gmail.com", 'm');
        Author  author2 = new Author("Hieu Ha","hieuhn@devcamp.vn", 'f');
        System.out.println("Author 1: " + author1.toString());
        System.out.println("Author 2: " + author2);

        Book book1 = new Book("Java coding in 24h", author1, 200.103);
        Book book2 = new Book("NodeJS coding in 24h", author2, 300.999,3);
        System.out.println("Book1: " + book1);
        System.out.println("Book2: " + book2);
        */
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle(3.0,"green");
        System.out.println("Circle1: " + circle1 +" has Area: " + circle1.getArea());
        System.out.println("Circle2: " + circle2 +" has Area: " + circle2.getArea());
        System.out.println("Circle3: " + circle3 +" has Area: " + circle3.getArea());

        Cylinder cylinder1 = new Cylinder();
        Cylinder cylinder2 = new Cylinder(2.5);
        Cylinder cylinder3 = new Cylinder(3.5,1.5);
        Cylinder cylinder4 = new Cylinder(3.5,1.5, "green");
        System.out.println("Cylinder1 has: " + cylinder1.getVolume());
        System.out.println("Cylinder2 has: " + cylinder2.getVolume());
        System.out.println("Cylinder3 has: " + cylinder3.getVolume());
        System.out.println("Cylinder4 has: " + cylinder4.getVolume());
    }
}
